aniso8601==7.0.0
attrs==19.1.0
certifi==2019.6.16
chardet==3.0.4
Click==7.0
Flask==1.1.1
flask-restplus==0.12.1
Flask-Script==2.0.6
Flask-Testing==0.7.1
idna==2.8
itsdangerous==1.1.0
Jinja2==2.10.1
jsonschema==3.0.2
MarkupSafe==1.1.1
pyrsistent==0.15.4
python-dateutil==2.8.0
pytz==2019.2
requests==2.22.0
six==1.12.0
urllib3==1.25.3
Werkzeug==0.15.5
