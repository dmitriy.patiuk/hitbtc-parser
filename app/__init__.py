from flask_restplus import Api
from flask import Blueprint

from .main.resources import api as transaction_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='HitBTC Parser',
          version='1.0',
          description='HitBTC transactions'
          )

api.add_namespace(transaction_ns, path='/txs')
