import os

basedir = os.path.abspath(os.path.dirname(__file__))


def environment_error(key):
    raise EnvironmentError(f'The environment variable `{key}` is not set.')


class Config:
    CLIENT_PUBLIC_KEY = os.getenv('CLIENT_PUBLIC_KEY') or environment_error('CLIENT_PUBLIC_KEY')
    CLIENT_SECRET_KEY = os.getenv('CLIENT_SECRET_KEY') or environment_error('CLIENT_SECRET_KEY')
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)
