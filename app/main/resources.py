from flask_restplus import Resource

from .utils import TransactionDto
from .services import TransactionService

api = TransactionDto.api
_transaction = TransactionDto.transaction


@api.route('/')
class TransactionList(Resource):
    @api.doc('list_of_transactions')
    @api.marshal_list_with(_transaction)
    def get(self):
        """List all transactions"""
        return TransactionService.get_all()
