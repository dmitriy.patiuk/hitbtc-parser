from .utils import Trades, AccountTransactions
from . import client


class TransactionService:
    apis = AccountTransactions, Trades

    @classmethod
    def get_all(cls):
        data = []
        for api in cls.apis:
            data.extend(api(client).get_data())
        return data
