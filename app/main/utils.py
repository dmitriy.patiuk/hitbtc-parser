from abc import ABC, abstractmethod

import requests

from dateutil.parser import parse
from flask_restplus import Namespace, fields


class TransactionDto:
    api = Namespace('transactions', description='transactions related operations')
    transaction = api.model('transaction', {
        'timestamp': fields.String(required=True, description='unix timestamp'),
        'assetName': fields.String(required=True,
                                   description='symbol field for trades and currency field for account transactions'),
        'type': fields.String(required=True, description='side field for trades and type for account transactions'),
        'quantity': fields.String(description='quantity field for trades, amount field for transactions'),
        'price': fields.String(description='price field for trades, 1 for account transactions'),
        'fee': fields.String(description='fee'),
    })


class Client:
    """
    Class for getting data from urls with session authentication
    """
    def __init__(self, public_key=None, secret_key=None):
        self.session = requests.session()
        self.session.auth = (public_key, secret_key)

    def init_app(self, app):
        """
        Initialization via flask application
        """
        self.session.auth = (app.config['CLIENT_PUBLIC_KEY'], app.config['CLIENT_SECRET_KEY'])

    def get(self, url):
        response = self.session.get(url)
        response.raise_for_status()
        return response.json()


def to_timestamp(value):
    _datetime = parse(value)
    return _datetime.timestamp()


class TransactionApi(ABC):
    """
    Abstract class for getting transaction data from different endpoints
    """
    URL = None

    def __init__(self, client):
        self.client = client

    def get_api_data(self):
        return self.client.get(self.URL)

    @abstractmethod
    def get_data(self):
        ...


class AccountTransactions(TransactionApi):

    URL = 'https://api.hitbtc.com/api/2/account/transactions/'

    def get_data(self):
        transactions = self.get_api_data()
        data = [
            {
                'timestamp': to_timestamp(transaction['createdAt']),
                'assetName': transaction['currency'],
                'quantity': transaction['amount'],
                'type': transaction['type'],
                'price': 1,
                'fee': transaction.get('fee'),
            } for transaction in transactions
        ]
        return data


class Trades(TransactionApi):

    URL = 'https://api.hitbtc.com/api/2/history/trades'

    def get_data(self):
        trades = self.get_api_data()
        data = [
            {
                'timestamp': to_timestamp(trade['timestamp']),
                'assetName': trade['symbol'],
                'type': trade['side'],
                'quantity': trade['quantity'],
                'price': trade['price'],
                'fee': trade.get('fee'),
            } for trade in trades
        ]
        return data
