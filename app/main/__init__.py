from flask import Flask

from .config import config_by_name
from .utils import Client

client = Client()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    client.init_app(app)
    return app
