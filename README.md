HitBTC Parser
========================

To begin you should have the following applications installed on your
local development system:

- Python >= 3.7
- `pip <http://www.pip-installer.org/>`_ >= 18.0
- `virtualenv <http://www.virtualenv.org/>`_ >= 15.1.0
- `virtualenvwrapper <http://pypi.python.org/pypi/virtualenvwrapper>`_ >= 4.8.2
- git >= 1.7

Getting Started
------------------------

First clone the repository from gitlab and switch to the new directory::

    $ git clone git@gitlab.com:dmitriy.patiuk/hitbtc-parser.git
    $ cd hitbtc-parser

To setup your local environment you should create a virtualenv and install the
necessary requirements:

    # Check that you have python3.7 installed
    $ which python3.7
    $ mkvirtualenv hitbtc -p `which python3.7`
    (hitbtc)$ pip install -r requirements.txt

Environment variables:
```
export CLIENT_PUBLIC_KEY="<KEY>"
export CLINET_SECRET_KEY="<SECRET>"
```

### Running tests

```
$ python manage.py test
```

### Run development server

```
$ python manage.py runserver
```

You're able to get access to the data by going to this URLs:

 - swagger: http://0.0.0.0:5000
 - endpoint: http://0.0.0.0:5000/txs/